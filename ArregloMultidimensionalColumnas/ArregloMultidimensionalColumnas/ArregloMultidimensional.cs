﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimensionalColumnas
{
    class ArregloMultidimensional
    {
        private int[,] numeros = new int[2, 5];

        public void SolicitarNumeros()
        {
            for (int j = 0; j < 5; j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    Console.Write($"Ingrese un número en la posicikón[{i},{j}]: ");
                    numeros[i, j] = int.Parse(Console.ReadLine());
                }
            }
        }

        public void MostrarResultados()
        {
            for (int j = 0; j < 5; j++)
            {
                for (int i = 0; i < 2; i++)
                {
                    Console.Write($"\nValor en la posición[{i},{j}] = {numeros[i, j]}");
                }
            }
        }
    }
}