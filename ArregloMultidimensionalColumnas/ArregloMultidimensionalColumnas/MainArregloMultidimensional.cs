﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArregloMultidimensionalColumnas
{
    class MainArregloMultidimensional
    {
        static void Main(string[] args)
        {
            ArregloMultidimensional a = new ArregloMultidimensional();

            a.SolicitarNumeros();

            a.MostrarResultados();

            Console.ReadKey();
        }
    }
}